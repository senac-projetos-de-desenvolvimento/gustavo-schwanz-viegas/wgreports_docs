# WGReports

## Descrição do projeto

> Este projeto visa facilitar e melhorar em vários pontos a utilização da metodologia scrum.
>
> Clique [aqui](https://www.figma.com/file/xsJD0GuSBwKqe9f7JG0Pz6/WGReports?node-id=1%3A4) para abrir o figma e ver o design utilizado.

## Status do Projeto

> O encontra-se em desenvolvimento. Até o momento não temos uma versão para produção.
>
> > Versões
> > --  
> > v0.0.0 -> Cria base do projeto e sobe para o repositório.

## Documentação:

> [Requisitos da aplicação e regras de negócio](https://gitlab.com/senac-projetos-de-desenvolvimento/gustavo-schwanz-viegas/wgreports_docs/-/wikis/Requisitos-da-aplica%C3%A7%C3%A3o-e-regras-de-neg%C3%B3cio)

> [Diagrama de casos de uso](https://gitlab.com/senac-projetos-de-desenvolvimento/gustavo-schwanz-viegas/wgreports_docs/-/wikis/Diagrama-de-caso-de-uso)

> [Sistemas similares](https://docs.google.com/document/d/1XRuqQUx-422DUmC6Gen5v1PfXXygQhNxjAEyt1VitlA/edit?usp=sharing)

> [Níveis de acesso](https://gitlab.com/senac-projetos-de-desenvolvimento/gustavo-schwanz-viegas/wgreports_docs/-/wikis/Niveis-de-acesso)

> [Estado da arte](https://docs.google.com/document/d/1IY_EXWm1WOVgpapKc_Ud1nyvQL9JgVbcccdrSaWBeiw/edit?usp=sharing)

> [Endpoits API](https://gitlab.com/senac-projetos-de-desenvolvimento/gustavo-schwanz-viegas/wgreports_docs/-/wikis/Documenta%C3%A7%C3%A3o-dos-endpoits-da-release-v1.0)

> [UML](https://gitlab.com/senac-projetos-de-desenvolvimento/gustavo-schwanz-viegas/wgreports_docs/-/wikis/UML-do-banco-de-dados)

## Tecnologias utilizadas

> Api
>
> > -   Express
> > -   Mysql
> > -   Docker
>
> Web
>
> > -   Nuxt
> > -   Tailwind
> > -   Heroicons
> > -   Docker

## Autores e Contribuição

> Alunos
>
> > -   Gustavo S. Viegas || Takewi
>
> Professor
>
> > -   Angelo Luz || angelogl
